﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Bacchus.Repositories;

namespace Bacchus.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/v1")]
    public class ProductsController : Controller
    {
        private readonly IProductsRepository _products;

        public ProductsController(IProductsRepository productsRepository)
        {
            _products = productsRepository;
        }

        [HttpGet("categories")]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> GetCategoriesAsync()
        {
            return Ok(await _products.GetCategoriesAsync());
        }

        [HttpGet("products/{category}")]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> GetProductsByCategoryAsync(string category)
        {
            return Ok(await _products.GetProductsAsync(category));
        }
    }
}