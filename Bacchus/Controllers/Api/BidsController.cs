﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bacchus.DTOs;
using Bacchus.Repositories;

namespace Bacchus.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/v1")]
    public class BidsController : Controller
    {
        private readonly IBidsRepository _bids;

        public BidsController(IBidsRepository bidsRepository)
        {
            _bids = bidsRepository;
        }

        [HttpGet("bids")]
        public IActionResult Get()
        {
            return Ok(_bids.GetBids());
        }

        [HttpPost("bid")]
        public IActionResult Post([FromBody] BidDTO bid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _bids.AddBid(bid);
            return Ok();
        }
    }
}