﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bacchus.DTOs;

namespace Bacchus.Repositories
{
    public interface IProductsRepository
    {
        Task<IEnumerable<ProductDTO>> GetProductsAsync(string category);
        Task<IEnumerable<CategoryDTO>> GetCategoriesAsync();
    }
}
