﻿using Bacchus.DTOs;
using Bacchus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Repositories
{
    public interface IBidsRepository
    {
        IEnumerable<Bidding> GetBids();
        void AddBid(BidDTO bid);
    }
}
