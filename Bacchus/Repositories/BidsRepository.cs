﻿using Bacchus.DTOs;
using Bacchus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Repositories
{
    public class BidsRepository : IBidsRepository
    {
        private readonly BiddingDbContext _db;

        public BidsRepository(BiddingDbContext context)
        {
            _db = context;
        }

        public IEnumerable<Bidding> GetBids()
        {
            return _db.Biddings;
        }

        public void AddBid(BidDTO bid)
        {
            var addBid = new Bidding
            {
                ProductId = bid.ProductId,
                Fullname = bid.Fullname,
                Amount = bid.Amount,
                DateTimeUtc = DateTime.UtcNow
            };
            _db.Biddings.Add(addBid);
            _db.SaveChanges();
        }
    }
}
