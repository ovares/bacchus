﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Bacchus.DTOs;
using Newtonsoft.Json;

namespace Bacchus.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        public async Task<IEnumerable<ProductDTO>> GetProductsAsync(string category)
        {
            HttpClient myClient = new HttpClient();
            HttpResponseMessage ret = await myClient.GetAsync("http://uptime-auction-api.azurewebsites.net/api/Auction");
            string str = ret.Content.ReadAsStringAsync().Result;
            var productList = JsonConvert.DeserializeObject<List<ProductDTO>>(str);
            productList = category == "All" ? productList : productList.Where(p => p.ProductCategory == category).ToList();
            foreach (var p in productList)
            {
                p.TimeLeft = (int)(p.BiddingEndDate - DateTime.UtcNow).TotalSeconds;
            }
            return productList.Where(p => p.BiddingEndDate >= DateTime.UtcNow);
        }

        public async Task<IEnumerable<CategoryDTO>> GetCategoriesAsync()
        {
            HttpClient myClient = new HttpClient();
            HttpResponseMessage ret = await myClient.GetAsync("http://uptime-auction-api.azurewebsites.net/api/Auction");
            string str = ret.Content.ReadAsStringAsync().Result;
            var productList = JsonConvert.DeserializeObject<List<ProductDTO>>(str);
            return productList.GroupBy(c => c.ProductCategory, c => c.ProductName, (key, g) => new CategoryDTO { ProductCategory = key });
        }
    }
}
