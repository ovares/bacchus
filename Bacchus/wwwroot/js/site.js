﻿var app = angular.module('bacchusApp', []);

app.filter('secondsToMmSs', function () {
    return function (seconds) {
        var minutes = Math.floor(seconds / 60);
        var scnds = seconds - (minutes * 60);
        return minutes + ":" + (scnds < 10 ? "0" + scnds : scnds);
    }
});

app.controller('ProductsController', function ($scope, $http) {
    $scope.selectedCategory = 'All';
    $http.get('/api/v1/categories').success(function (response) {
        $scope.categories = response;
    });

    $http.get('/api/v1/products/All').success(function (response) {
        $scope.products = response;
        $scope.selectedProduct = $scope.products[0].productId;
    });

    $scope.update = function () {
        $http.get('/api/v1/products/' + $scope.selectedCategory).success(function (response) {
            $scope.products = response;
        });
    };

    $scope.bid_button = function (product) {
        if ($scope.newbid.fullname.length === 0 || typeof $scope.newbid.fullname === 'undefined') {
            alert('Please enter your full name.');
        } else {
            $scope.newbid.productId = product.productId;
            $scope.newbid.amount = product.amount;
            $http.post('/api/v1/bid', $scope.newbid).success(function (response) {
                $http.get('/api/v1/products/' + $scope.selectedCategory).success(function (response) {
                    $scope.products = response;
                });
            });
            alert('Bid placed ' + product.amount);
        }
    };
});

app.controller('BiddinglistController', function ($scope, $http) {
    $http.get('/api/v1/bids').success(function (response) {
        $scope.bids = response;
    });
});