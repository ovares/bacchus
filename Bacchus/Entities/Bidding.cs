﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.Entities
{
    public class Bidding
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCategory { get; set; }
        public string Fullname { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateTimeUtc { get; set; }
    }
}
