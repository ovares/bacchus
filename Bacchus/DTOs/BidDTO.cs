﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.DTOs
{
    public class BidDTO
    {
        public string ProductId { get; set; }
        public string Fullname { get; set; }
        public decimal Amount { get; set; }
    }
}
