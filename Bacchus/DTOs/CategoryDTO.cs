﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bacchus.DTOs
{
    public class CategoryDTO
    {
        public string ProductCategory { get; set; }
    }
}
