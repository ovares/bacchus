﻿CREATE TABLE [dbo].[Bidding](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [nvarchar](max) NOT NULL,
	[ProductName] [nvarchar](max) NULL,
	[ProductCategory] [nvarchar](max) NULL,
	[Fullname] [nvarchar](max) NOT NULL,
	[Amount] [decimal](10, 2) NOT NULL,
	[DateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_Bidding] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)